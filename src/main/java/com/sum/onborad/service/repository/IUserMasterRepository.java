package com.sum.onborad.service.repository;

import org.springframework.data.repository.CrudRepository;

import com.sum.onborad.service.model.TbUserMaster;

public interface IUserMasterRepository extends CrudRepository<TbUserMaster, Long>{


	TbUserMaster findBynUserId(Long nUserId);
	
	
	
}
