package com.sum.onborad.service.service;

import org.springframework.stereotype.Service;

import com.sum.onborad.service.dto.InputUserDetailsDto;
import com.sum.onborad.service.dto.OutputResponseDto;


@Service
public interface IUserMgmtService {
	public OutputResponseDto getAllUserDetails(Long nUserId);

	public OutputResponseDto registerUserDetails(InputUserDetailsDto inputUserDetailsDto);


}
